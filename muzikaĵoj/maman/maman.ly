\include "../ĉiea.ly"

\header {
  titolo-xx     = "Ah~! vous dirai-je, maman"
  titolo-he     = "שאגיד לך, אמא?"
  titolo-eo     = "ABC"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🔤"
}

\include "../titolo.ly"

melodio = {
   \key d \major
   \time 4/4
   d4 d4 a4 a4 
   b4 b4 a2 
   g4 g4 fis4 fis4 
   e4 e4 d2 
   \break
   a4 a4 g4 g4 
   fis4 fis4 e2 
   a4 a4 g4 g4 
   fis4 fis4 e2 
   \break
   d4 d4 a4 a4 
   b4 b4 a2 
   g4 g4 fis4 fis4 
   e4 e4 d2 
   \bar "|."
}

\include "../muziko.ly"
