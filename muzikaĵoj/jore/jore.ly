\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "היורה"
  titolo-eo     = "La unua pluvo"
  komponisto-xx = ""
  komponisto-he = "יואל אנגל"
  komponisto-eo = "Joel Engel"
  ikono         = "☔"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \major
  d' a8 r b4 fis8 r |
  d' d' a a b b fis fis |
  d' d' a a b b fis fis |
  g fis e4 fis8 g a4 |
  d'8 cis' b4 cis'8 d' e'4 |
  d'8 r cis' r b r a r |
  d' d' cis' r b b a r |
  d'4 e' fis' r |
  e' a d' r |
  \bar "|."
}

\include "../muziko.ly"
