\include "../ĉiea.ly"

\header {
  titolo-xx = ""
  titolo-he = "ההוביט: מעבר להרי הערפל הקרים"
  titolo-eo = "Trans la malvarmaj Nebulaj Montoj"
  komponisto-xx = ""
  komponisto-he = "הווארד שור"
  komponisto-eo = "Howard Shore"
  ikono = "💰"
}

\include "../titolo.ly"

melodio =  {
  \key b \minor
  \time 4/4 | 
  \partial 4*2 fis4 a4 | 
  b2. d'4 | 
  e'4 fis'8  e'8  d'4 cis'4 | 
  b1 | 
  \break
  r4 fis4 b4 cis'4 | 
  cis'1 ~ | 
  cis'4 d'4 e'4 d'8  cis'8  | 
  b1 |
  \break
  r2 a4 cis'4 | 
  b2. d'4 | 
  e'4 fis'8  d'8  d'4 cis'4 | 
  b1 | 
  \break
  r4 fis4 b4 cis'4 | 
  cis'1 ~ |
  cis'4 d'4 e'4 d'8  cis'8  | 
  b1 | 
  \break
  r4 d'4 e'4. cis'8 | 
  fis'1 ~ |
  fis'4 d'4 e'4. b8 | 
  cis'1 ~ | 
  \break
  cis'4 fis4 a4 cis'4 | 
  d'1 ~ | 
  d'4 e'8  d'8 cis'4 a4 | 
  b1 | 
  \break
  r4 fis4 fis4 a4 | 
  b4 b2. | 
  d'4 e'8  d'8  cis'4 a4 | 
  b1 |
  \break
  r4 fis4 b4 cis'4 | 
  cis'4 cis'2. | 
  d'4 e'8  d'8  cis'4 b4 | 
  cis'1 | 
  \break
  r4 d'4 e'4. cis'8 | 
  fis'1 ~ | 
  fis'4 d'4 e'4. b8 | 
  cis'1 ~ | 
  \break
  cis'4 fis4 a4 cis'4 | 
  d'4 d'2. |
  cis'4. a8 b2 ~ | 
  b1 \bar "|." |
}

\include "../muziko.ly"
