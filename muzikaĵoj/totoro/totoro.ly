\include "../ĉiea.ly"

\header {
  titolo-xx     = "となりのトトロ"
  titolo-he     = "השכן שלי טוטורו"
  titolo-eo     = "Mia najbaro Totoro"
  komponisto-xx = "久石譲"
  komponisto-he = "ג׳ו היסאישי"
  komponisto-eo = "Ĝo Hisaiŝi"
  ikono         = "🌳"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key g \major
  g fis g8 d4.~ | 
  d1 | 
  g4 fis g8 b4.~ | 
  b2. c'4 | 
  b a g8 c'4. | 
  b4 a g g~ | 
  g8 a a2~ a8 r8 | 
  g4 fis g8 d4.~ | 
  d2. r4 | 
  g4 fis g8 d'4.~ | 
  d'2 r4. c'8 | 
  c'8 c' c'4 b8 a c'4~ | 
  c'4. a8 b8 c' b4 | 
  b4 b8 a g8 b4.~ | 
  b2 r4 e | 
  fis4 g a8 e4 e8 | 
  fis4 g8 a g d'4.~ | 
  d'1~ | 
  d'8 r g a b c' d'4 | 
  b8 g4 d' c' a8~ | 
  a4 r4 c'4 a8 fis~ | 
  fis8 c'4 b g4. | 
  r4 dis g c' | 
  b4 d'8 g4 r4 b8 | 
  c'8 b c' b c' b g a~ | 
  a4 r4 g8 a b c' | 
  d'4 b8 g4 d' c'8~ | 
  c'8 a4. r4 c'4 | 
  a8 fis4 c' b g8~ | 
  g4 r4 e4 e' | 
  d'8 c' b c' d'4. g8 | 
  g4 r8 g8 c' b g c' | 
  b8 g e' d'2~ d'8~ | 
  d'2 r4 d8 d | 
  c'8 b a b g2~ | 
  g1 \bar "|."
}

\include "../muziko.ly"
