\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "לכובע שלי"
  titolo-eo     = "Mia ĉapelo"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "△"
}

\include "../titolo.ly"

melodio = {
   \key g \major
   \time 3/4
   \partial 4 {d4}
   g4 g4 a4
   b2 d4
   g2 b4
   a2 d4
   \break
   a2 b4
   c'4 a4 fis4
   d4 e4 fis4
   g2 d4
   \break
   g2 a4
   b4 b4 d4
   g2 b4
   a2.
   \break
   a2 b4
   c'4 a4 fis4
   d4 e4 fis4
   g2.
   \bar "|."
}

\include "../muziko.ly"
