\include "../ĉiea.ly"

\header {
  titolo       = "Happy Birthday to You"
  titolo-he    = "יום הולדת שמח"
  titolo-eo    = "Feliĉan naskiĝtagon"
  komponisto    = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono      = "🎈"
}

\include "../titolo.ly"

melodio = {
  \key g \major
  \time 3/4
  \partial 4 d8 d8 | 
  e4 d4 g4 | 
  fis2 d8 d8 | 
  e4 d4 a4 |
  g2 d8 d8 |
  \break
  d'4 b4 g4 | 
  fis4 e4 c'8 c'8 | 
  b4 g4 a4 | 
  g2. | 
  \bar "|."
}

\include "../muziko.ly"
