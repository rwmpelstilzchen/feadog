\include "../ĉiea.ly"

\header {
  titolo-xx     = "Quen a omagen da Virgen"
  titolo-he     = "קנטיגה 353"
  titolo-eo     = "Kantigo 353"
  komponisto-xx = ""
  komponisto-he = "עממי / אלפונזו העשירי"
  komponisto-eo = "popola / Alfonso la 10-a"
  ikono         = "♍"
}

\include "../titolo.ly"

melodio = {
  \tempo 4=180
  \key e \dorian
  \time 4/4
  e4 b4 b4. a8
  g4 g8 a8 b4. e8
  e4 b4 b4. a8
  g8 fis8 e4 g2
  e4 b4 b4. a8
  g4 g8 a8 b4. e8
  e4 b4 b4. a8
  g8 fis8 e8 d8 e2
  \bar "||"
  \break
  b4 cis'4 a4. b8
  cis'4 d'8 cis'8 b4. a8
  b4 b8 cis'8 b4. a8
  g4 a4 g2
  b4 cis'4 a4. b8
  cis'4 d'8 cis'8 b4. a8
  b4 b8 cis'8 b4. a8
  g8 fis8 e8 d8 e2
  \bar "|."
}

\include "../muziko.ly"
