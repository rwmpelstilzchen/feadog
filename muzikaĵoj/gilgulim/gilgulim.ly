\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "פלא: גלגולים"
  titolo-eo     = "Mirindaĵo: Aliformiĝoj"
  komponisto-xx = ""
  komponisto-he = "עלמה זהר"
  komponisto-eo = "Alma Zohar"
  ikono         = "🐜"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 4/4
  d8 d4 e8 fis8 fis4. |
  e4 e4 d2 |
  fis8 fis8 fis8 g8 a4 a4 |
  g8 fis g b a2 |
  \break
  b8 b b cis' d'4 d' |
  d8 d8 d8 e8 fis2 |
  d8 d4 e8 fis4 fis4 |
  e4 e4 d2 |
  \break
  d'8 a d' a4. a8 d' |
  d'8 cis' d'4 a2 |
  d'8 a d' a4. a8 d' |
  d'8 cis' d'2. | \bar "|."
}

\include "../muziko.ly"