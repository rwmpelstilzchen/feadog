\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "בובה קטנה"
  titolo-eo     = "La pupeto"
  komponisto-xx = ""
  komponisto-he = "יודה רונן"
  komponisto-eo = "Júda Ronén"
  ikono         = "🎎"
}

\include "../titolo.ly"

melodio = {
%  \key fis \minor
%  \time 4/4
%  cis'8 b cis'4 fis4 fis |
%  d'8 cis' d' cis' b2 |
%  b8 a b4 e e |
%  cis'8 b cis' b a2 |
%  a8 gis a4 d d |
%  b8 a b a gis2 |
%  gis4 fis fis fis ~ |
%  fis8 f fis2. |
%  \bar "||"
%  %cis'8 b cis'4 dis' e' ~|
%  %e'8 e'8 d' cis' b2 |
%  cis'8 b cis'4 dis' e' ~|
%  e'8 e'8 d' cis' b2 |
%  d'4 cis' b8 a b4 |
%  gis4 a4. b8 cis' d' | 
%  e'4 d'4 cis' b8 a |
%  %b4 gis a2 |
%  %fis8 gis a4 fis gis |
%  %f4 fis2. |
%  b4 gis a4. gis8 |
%  fis8 gis a4 gis4 fis8 f | 
%  fis4 gis4 fis2 |
%  \bar "|."
  \key e \minor
  \time 4/4
  b8( a b4) e e |
  c'8 b c' b a2 |
  a8( g a4) d d |
  b8 a b a g2 |
  g8( fis g4) c c |
  a8 g a g fis2 |
  fis4 e e e ~ |
  e8 ees e2. |
  \bar "||"
  b8 a b4 cis' d' ~ |
  d'8 d' c' b a2 |
  c'4 b a8 g a4 |
  fis g4. a8 b c' |
  d'4 c' b a8 g |
  a4 fis g4. fis8 |
  e fis g4 fis e8 ees |
  e4 fis e2 |
  \bar "|."
 
}

\include "../muziko.ly"
