\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "עוגה עוגה"
  titolo-eo     = "Ronde, ronde"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🎂"
}

\include "../titolo.ly"

melodio = {
  \key g \major
  \time 4/4
  b4 b a a |
  g2 d |
  b4 b a a |
  g2 d |
  \break
  g4 g a a |
  b d' a2
  g4 g a a |
  b d' a2
  \break
  b4 g2 d4 |
  b g2. |
  b4 g2 d4 |
  b g2. |
  \break
  b4 g2 d4 |
  b g2. |
  d'4 d' c' b |
  a g2. |
  \bar "|."
}

\include "../muziko.ly"
