\include "../ĉiea.ly"

\paper { page-count = #2 }

\header {
  titolo-xx     = "借りぐらしのアリエッティ"
  titolo-he     = "הלקחנים"
  titolo-eo     = "La prenistoj"
  komponisto-xx = ""
  komponisto-he = "ססיל קוברל"
  komponisto-eo = "Cécile Corbel"
  ikono         = "📍"
}

\include "../titolo.ly"

melodio = {
  \key g \major
  \time 3/4

  \interim
  e4 e8 fis8 g8 b8 | 
  d4 d8 e8 fis8 a8 | 
  g4 e8 fis8 g8 b8 | 
  a4 g4 fis4 | 
  g4 e8 fis8 g8 b8 | 
  d4 d8 e8 fis8 a8 | 
  g4 e8 fis8 g8 b8 | 
  a4 g4 \deinterim e4 | 
  \break

  b4 b4 b4 |
  a4 a4 g4 | 
  b8 e8 ~ e2 | 
  r2 e4 | 
  \break
  b4 b4 b4 | 
  a4 ~ a8 g8 a8 g8 | 
  b4 d'2 ~ | 
  d'2 r4 | 
  \break
  r4 r8 b8 b8 e'8 | 
  d'4 a8 a4 g8 | 
  r4 r8 b8 b8 e'8 |
  d'4 a8 a4 g8 | 
  r2 b4 | 
  e2 b4 | 
  a4 a4 g4 | 
  a8 g8 e2 | 
  R2. | 
  \break
  r2 e'4 | 
  d'4 g4 d'4 | 
  e'4 a4 e'4 | 
  fis'4 ~ fis'8 e'8 d'4 |
  e'8 d'8 c'4 e'4 | 
  d'4 g4 d'4 | 
  e'4 d'4 c'8 d'8 | 
  d'4. r8 a8 c'8 |
  \break
  \key c \major d'4 d'8 c'8 d'8 g'8 | 
  e'4. r8 d'8 c'8 | 
  d'4 d'8 e'8 d'8 c'8 | 
  a4 g4 r4 | 
  \break
  d'8 d'8 d'8 e'8 d'8 c'8 | 
  a2 c'4 |
  d'2. | 
  r2 a8 c'8 | 
  \break
  d'4 d'8 c'8 d'8 g'8 | 
  e'4. r8 d'8 c'8 | 
  d'4 d'8 e'8 d'8 c'8 | 
  a4 g4 r4 | 
  \break
  d'8 d'8 d'8 e'8 d'8 c'8 | 
  a2 c'4 | 
  d'2. | 
  r2 a4 |
  \break
  e'4 e'4 e'4 | 
  d'4 d'4 c'4 | 
  e'8 a8 ~ a2 | 
  r2 a4 | 
  \break
  e'4 e'4 e'4 | 
  d'4 ~ d'8 c'8 d'8 c'8 | 
  e'4 g'2 ~ | 
  g'2 r4 | 
  \break
  r4 r8 e'8 e'8 a'8 | 
  g'4 d'8 d'4 c'8 |
  r4 r8 e'8 e'8 a'8 | 
  g'4 d'8 d'4 c'8 | 
  r2 e'4 | 
  a2 e'4 | 
  d'4 d'4 c'4 | 
  d'8 c'8 a2 | 
  R2. | 
  \break
  r2 e'4 | 
  \key g \major d'4 g4 d'4 | 
  e'4 a4 e'4 |
  fis'4 ~ fis'8 e'8 d'4 | 
  \break
  e'8 d'8 c'4 e'4 | 
  d'4 g4 d'4 | 
  e'4 d'4 c'8 d'8 | 
  d'4. r8 a8 c'8 | 
  \break
  d'4 d'8 c'8 d'8 g'8 | 
  e'4. r8 d'8 c'8 | 
  d'4 d'8 e'8 d'8 c'8 | 
  a4 g4 r4 | 
  \break
  d'8 d'8 d'8 e'8 d'8 c'8 |
  a2 c'4 | 
  d'2. | 
  r2 a8 c'8 | 
  \break
  d'8 a8 d'8 c'8 d'8 g'8 | 
  e'4. r8 d'8 c'8 | 
  d'4 d'8 e'8 d'8 c'8 | 
  a4 g4 r4 | 
  \break
  d'8 d'8 d'8 e'8 d'8 c'8 | 
  a2 c'4 | 
  d'2. |
  g4 b4. r8 | 
  a2. \bar "|."
}

\include "../muziko.ly"
