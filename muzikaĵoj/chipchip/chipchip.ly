\include "../ĉiea.ly"

\header {
  titolo-xx     = "Der Ententanz"
  titolo-he     = "ריקוד הציפורים"
  titolo-eo     = "Birdeta danco"
  komponisto-xx = ""
  komponisto-he = "ורנר תומס"
  komponisto-eo = "Werner Thomas"
  ikono         = "🐦"
}

\include "../titolo.ly"

melodio = {
   \key d \major
   \time 4/4
   \partial 8*6 { a8 a8 b8 b8 fis8 fis8 |}
   a4 a8 a8 b8 b8 fis8 fis8 |
   a4 a8 a8 b8 b8 d'8 d'8 |
   cis'4 cis'4 b4 a4 |
   g4 g8 g8 a8 a8 e8 e8 |
   \break
   g4 g8 g8 a8 a8 e8 e8 |
   g4 g8 g8 a8 a8 cis'8 cis'8 |
   b4 b4 a4 g4 |
   fis4 a8 a8 b8 b8 fis8 fis8 |
   \break
   a4 a8 a8 b8 b8 fis8 fis8 |
   a4 a8 a8 b8 b8 d'8 d'8 |
   cis'4 cis'4 b4 a4 |
   g4 g8 g8 a8 a8 e8 e8 |
   \break
   g4 g8 g8 a8 a8 e8 e8 |
   g4 a8 a8 b8 b8 cis'8 cis'8 |
   d'1~ |
   d'4 a4 gis4 g4 |
   \break
   fis2. a4 |
   fis'2. e'4 |
   e'2 d'2~ |
   d'4 a4 gis4 g4 |
   fis2. a4 |
   e'2. d'4 |
   cis'1~ |
   cis'4 b4 a4 gis4 |
   \break
   g2. a4 |
   e'2. d'4 |
   d'2 cis'2~ |
   cis'4 e'4 e'4 d'4 |
   d'2 cis'2~ |
   cis'4 cis'4 d'4 e'4 |
   d'1 |
   \bar "|."
}

\include "../muziko.ly"
