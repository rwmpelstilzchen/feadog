\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "נדנדה"
  titolo-eo     = "Baskulo"
  komponisto-xx = ""
  komponisto-he = "דניאל סמבורסקי"
  komponisto-eo = "Daniel Samburski"
  ikono         = "⚖"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 4/4
  d4 b g2 |
  d4 b g2 |
  a4. b8 a4 g |
  fis4 e fis d |
  \break
  d4 b g2 |
  d4 b g2 |
  a4. a8 b4 cis' |
  d'4 d' d'2 |
  \break
  a4. b8 a4 g |
  fis4 e fis d |
  a4. b8 a4 g |
  fis4 e fis d |
  \break
  d4 b g2 |
  d4 b g2 |
  a4. a8 b4 cis' |
  d'4 d' d'2 |
  \bar "|."
}

\include "../muziko.ly"
