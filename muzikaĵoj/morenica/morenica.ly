\include "../ĉiea.ly"

\header {
  titolo-xx     = "Morenica"
  titolo-he     = "שחרחורת"
  titolo-eo     = "Nigranjo"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "●"
}

\include "../titolo.ly"

melodio = {
  \key e \minor
  \time 2/4
  \partial 8*2 c'8 b |
  c'4 b |
  c'4 d' |
  e'2 |
  d'8 c' b a |
  g4 fis8 e |
  fis2 |
  c'2 |
  \break
  b4 a8 b |
  a4 g8 fis |
  g2 |
  a8 b16 c' b8 a |
  a2 ~ |
  a2 ~ |
  \break
  a4 c'8 b |
  c'4 b |
  c'4 d' |
  e'2 |
  d'8 c' b a |
  g4 fis8 e |
  fis2 |
  c'2 |
  \break
  b4 a8 b |
  a4 g8 fis |
  g2 |
  a8 b16 c' b8 a |
  a2 ~ |
  a2 ~ |
  a2 |
  \break
  b8 a g fis |
  e4. fis8 |
  d2 ~ |
  d2 |
  b8 a g fis |
  e8 g fis e |
  d2 ~ |
  \break
  d4 d8 d |
  e4 fis |
  g4 a |
  b4 c'8 d' |
  b4 a |
  g4. f8 |
  a4 f |
  e2 ~ |
  e2 |
  \bar "|."
}

\include "../muziko.ly"
