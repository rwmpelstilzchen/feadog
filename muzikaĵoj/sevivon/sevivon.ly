\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "סובב הסביבון"
  titolo-eo     = "La turbo turbas"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "⟳"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  d'4 b8 b b b c' b |
  a b g2.|
  d'4 b8 b b b c' b |
  a b g2. |
  \break
  d8 b a2. |
  d8 b a2. |
  a8 b cis' d'8 ~ d'2 |
  R1 |
  \break
  e'4 d'2 b4 |
  e'4 d'2 b4 |
  e'4 d' d' b |
  e' d'2. |
  \break
  d'4 b8 b b b c' b |
  a b g2.|
  d'4 b8 b b b c' b |
  a b g2. |
  \break
  d8 b a2. |
  d8 b a2. |
  a8 b cis' d'8 ~ d'2 ~ |
  d'1 |
  \bar "|."
}

\include "../muziko.ly"
