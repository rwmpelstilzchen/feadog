\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אצא לי אל היער"
  titolo-eo     = "Mi venos al la arbaro"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🌲"
}

\include "../titolo.ly"

\language "italiano"
melodio = {
  \key sol \major
  \time 4/4
  \partial 4 re4 |
  sol4 sol sol si |
  sol2 re4 si |
  la4. sol8 la4 si |
  sol2. re4 |
  \break
  sol4 sol sol si |
  sol2 re4 si |
  la4. sol8 la4 si |
  sol2. re'4 |
  \break
  si2. re'4 |
  si2. re'4 |
  do'4. si8 do'4 re' |
  si2. re'4 |
  \break
  si2. re'4 |
  si2. re'4 |
  do'4. si8 do'4 la |
  sol2. \bar "|." |
}
\language "nederlands"

\include "../muziko.ly"
