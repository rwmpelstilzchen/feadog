\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "קן לציפור"
  titolo-eo     = "Birdonesto"
  komponisto-xx = ""
  komponisto-he = "יצחק אדל"
  komponisto-eo = "Jicĥak Edel"
  ikono         = "🐣"
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 80
  \time 4/4
  \key g \major
  b8 c' d' c' b4 a |
  a8 g a b a2 |
  a8 b c' a a4 g |
  g8 fis g a b2 |
  b8 a8 b c'8 d'2 |
  c'8 b c' d' e'4. e'8 |
  d'4 c'8 b a4 e8 e |
  b8 a g a g2 |
  \bar "|."
}

\include "../muziko.ly"
