\include "../ĉiea.ly"

\header {
  titolo-xx     = "Frozen: Let it Go"
  titolo-he     = "לשבור את הקרח: לשחרר"
  titolo-eo     = "Frosta: Liberu"
  komponisto-xx = ""
  komponisto-he = "קריסטן אנדרסון-לופז ורוברט לופז"
  komponisto-eo = "Kristen Anderson-Lopez kaj Robert Lopez"
  ikono         = "🌨"
}

\include "../titolo.ly"

melodio = {
  \compressFullBarRests
  \key g \major
  \time 4/4
  \interim
  fis'8 g' b fis' ~ fis' g'4. |
  fis'8 g' b g' ~ g' fis'4 a8 |
  e' fis' a e' ~ e' fis'4 a8 |
  d'2 c' |
  \break
  fis'8 g' b fis' ~ fis' g'4. |
  g'8 fis' b g' ~ g' fis'4 a8 |
  e' fis' a e' ~ e' fis'4 a8 |
  d'2 cis'4. \deinterim a8 |
  \break
  b4 b b b8 b |
  b a g g ~ g4 g8 g |
  a a4. ~ a8 g fis e ~ |
  e2 r4 r8 e |
  \break
  b b b b ~ b4 d'8 d' |
  b ~ b4. r4 g8 g |
  a b4. r8 a g a ~ |
  a2 r |
  \break
  r4 b8 b ~ b d'4 e'8 ~ |
  e' d'4 b8 ~ b d'4 d'8 ~ |
  d' d'4 c'8 b b4 c'8 |
  b1 |
  \break
  r4 b8 b a g4 a8 ~ |
  a4 a8 b a4 g ~ |
  g e2. ~ |
  e2 r |
  \break
  r4 a8 a ~ a d'4 d'8 ~ |
  d' a4 a8 ~ a e'4 e'8 |
  r4 e'8 d' e' e'4 d'8 |
  e' fis'4 g'8 ~ g' fis'4 fis'8 |
  r4 a8 a ~ a d'4 d'8 |
  r4 a8 a ~ a e'4 e'8 ~ |
  e'1 ~ |
  e'2 r4 d'8 e' ~ |
  e' fis'4. g'2 ~ |
  g' r8 e' fis' g' ~ |
  g'2 r8 d' d' a' ~ |
  a'2. g'4 |
  e'8 e' e'4 e'8 fis'4 g'8 ~ |
  g'2 r8 e' fis' g' ~ |
  g'2 g'8 d' b' a' ~ |
  a'2 r8 g' a' b' ~ |
  b' b'4 c''8 b'4 a'8 g' |
  a' g'2. r8 |
  d''4. b' a'4 ~ |
  a'2 d'4 d' |
  d''4. b' g'4 ~ |
  g'4. r8 r4 g'8 g' |
  fis'4. d' d'4 ~ |
  d'2 r4 r8 r |
  c'4 c'8 b c' b c' c' |
  b g4. r2 |
  R1*2 |
  \break
  r8 b b b b b4 b8 ~ |
  b g4. r4 g |
  d' d'8 c' ~ c' b4 a8 ~ |
  a2 r4 g8 g |
  \break
  b4 b8 b ~ b d'4 e'8 ~ |
  e'4 d' r d' |
  g' g'8 fis' e' e'4 e'8 ~ |
  e'1 |
  \break
  r4 a8 a ~ a d'4 d'8 ~ |
  d' a4 a8 ~ a d'4 e'8 |
  r4 d'8 e' ~ e' d'4 e'8 ~ |
  e' fis'4 g'8 ~ g' a'4 d'8 ~ |
  d'4 a8 a ~ a d'4 d'8 ~ |
  d' a4 d'8 ~ d' a e'4 ~ |
  e'2 r8 fis'4. |
  g'1 |
  R |
  \break
  r2 r8 e' fis' g' ~ |
  g'2 r8 d' d' a' ~ |
  a'2 r8 g' e' e' ~ |
  e' e' e' e'4 fis' g'8 ~ |
  g' a' g'4 r8 e' fis' g' ~ |
  g'2 r8 d' b' a' ~ |
  a'2 r4 g'8 b' ~ |
  b' b'4 c''4. b'8 a' |
  a' g'4. r2 |
  d''4. b' a'4 ~ |
  a'2. d'4 |
  d''4. b' g'4 ~ |
  g'4. r8 r4 g'8 g' |
  fis'4. d' d'4 ~ |
  d'2 r |
  R1*4 |
  \break
  r4 c'8 b ~ b c'4 d'8 ~ |
  d' e'4 f'8 ~ f' g'4 bes'8 ~ |
  bes' a'4 g'8 ~ g' f'4 g'8 ~ |
  g'1 |
  \break
  r4 c'8 b ~ b c'4 d'8 ~ |
  d' e'4 f'8 ~ f' g'4 bes'8 ~ |
  bes' a'4 g'8 ~ g' f'4 bes'8 ~ |
  bes' a' g' a' g'2 |
  \break
  r4 d'8 cis' ~ cis' d'4 e'8 ~ |
  e' fis'4 g'8 ~ g' a'4 c''8 ~ |
  c'' b'4 a'8 ~ a' g'4 a'8 ~ |
  a'1 |
  \break
  r4 b'8 b' ~ b' b'4 b'8 ~ |
  b'4 g'8 g' ~ g'4 r8 g' |
  a'4 g'8 a' a' b'4 c''8 ~ |
  c''1 ~ |
  \break
  c''2 ~ c''8 c'' c'' c'' |
  b'2 r8 d' d' a' ~ |
  a'2 r8 g' e' e' ~ |
  e' e' e' e' ~ e' fis'4 g'8 ~ |
  g' a' g'4 r8 e' fis' g' ~ |
  g'2 r8 d' b' b' |
  a'2 r8 g'4 b'8 ~ |
  b' b'4 c''8 ~ c'' b' a' g' |
  a' g'4. ~ g'4 r |
  d'' b'8 b' b'4 a' ~ |
  a'2 g'4 g' |
  d'' b'8 b' ~ b'4 c''8 b' ~ |
  b' a' g'2. ~ |
  g'2 r4 g'8 g' |
  fis'4. g'8 ~ g'4 d'' ~ |
  d''1 ~ |
  d''2. r8 bes |
  c'4 c'8 b c' b c' c' |
  b g4. r2 |
  \bar "|."
}   

\include "../muziko.ly"
