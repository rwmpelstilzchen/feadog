\include "../ĉiea.ly"

\header {
  titolo-xx     = \markup{\small{Como somos per conssello do démo perdudos}}
  titolo-he     = "קנטיגה 119"
  titolo-eo     = "Kantigo 119"
  komponisto-xx = ""
  komponisto-he = "עממי / אלפונזו העשירי"
  komponisto-eo = "popola / Alfonso la 10-a"
  ikono         = "♍"
}

\include "../titolo.ly"

melodio = {
  \key e \dorian
  \time 3/4
  g4 g8 g4 e8 |
  g4 g8 g4 a8 |
  b4 a8 g8. fis16 e8 |
  d4 d4. e8 |
  fis4 fis8 fis4 e8 |
  fis4 fis8 fis4 e8 |
  g4 g16 fis16 e4 d8 |
  e4 e2 | \bar "||"
  \break
  b4 b8 a4 cis'8 |
  d'4 cis'8 b4 a8 |
  g4 fis8 e4 d8 |
  g4 g2 |
  b4 b8 a4 cis'8 |
  d'4 cis'8 b4 a8 |
  g4 fis8 e4 d8 |
  e4 e2 | \bar "|."
}

\include "../muziko.ly"
