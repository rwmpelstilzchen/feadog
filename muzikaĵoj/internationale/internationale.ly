\include "../ĉiea.ly"

\header {
  titolo-xx     = "L’Internationale"
  titolo-he     = "האינטרנציונל"
  titolo-eo     = "La Internacio"
  komponisto-xx = ""
  komponisto-he = "פייר דגייטר"
  komponisto-eo = "Pierre De Geyter"
  ikono         = "⚖"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \major
  \partial 8 a8 |
  d'4. cis'8 e' d' a fis |
  b2 g4. b8 |
  e'4. d'8 cis' b a g |
  fis2 r4 a |
  \break
  d'4. cis'8 e' d' a fis |
  b2 g4 e'8 d' |
  cis'4 e' g' cis' |
  d'2 r4 fis'8 e' |
  \break
  cis'2 b8 cis' d' b |
  cis'2 a4 gis8 a |
  b4. e8 e'4. d'8 |
  cis'2 r4 r8 e' |
  \break
  e'4. cis'8 a a gis a |
  fis'2 d'8 d' cis' b |
  cis'4 e' d' b |
  a r r fis'8 e' |
  \break
  d'2 a4. fis8 |
  b2 g4 e'8 d' |
  cis'2 b4. a8 |
  fis'2 r4 fis' |
  \break
  fis'2 e'4. a8 |
  d'2 cis'4. cis'8 |
  b4. ais8 b4 e' |
  e'2 r4 fis'8 e' |
  \break
  d'2 a4. fis8 |
  b2 g4 e'8 d' |
  cis'2 b4. a8 |
  fis'2 r4 fis' |
  \break
  a'2 g'4 fis' |
  e'8 dis' e' fis' g'4 r8 g' |
  fis'4. d'8 e'4. cis'8 |
  d'2 r4 r4 |
  \bar "|."
}

\include "../muziko.ly"
