\include "../ĉiea.ly"

\header {
  titolo-xx     = "魔女の宅急便：晴れた日に"
  titolo-he     = "קיקי: ביום בהיר"
  titolo-eo     = "Kiki: ĉe klara tago"
  komponisto-xx = ""
  komponisto-he = "ג׳ו היסאישי"
  komponisto-eo = "Ĝo Hisaiŝi"
  ikono         = "🎀"
}

\include "../titolo.ly"

melodio = {
  \key g \major
  \time 3/4 
  \tempo 4=140
  d'2. | % 2
  des'2 e'4 | % 3
  d'2. ~ | % 4
  d'2. | % 5
  b2. | % 6
  bes2 c'4 | % 7
  b2. ~ | % 8
  b2. | % 9
  \break
  g2. |
  fis2 a4 | % 11
  g2. | % 12
  fis4 g4 a4 | % 13
  b2 a4 ~ | % 14
  a2 e4 | % 15
  a2. ~ | % 16
  a2. | % 17
  \break
  d'2. | % 18
  des'2 e'4 | % 19
  d'2. ~ |
  d'2. | % 21
  b2. | % 22
  bes2 c'4 | % 23
  b2. ~ | % 24
  b2. | % 25
  \break
  g2. | % 26
  fis2 a4 | % 27
  g2 b4 | % 28
  d'2 c'4 | % 29
  b2. |
  a4 fis4 a4 | % 31
  g2. ~ | % 32
  g2. | % 33
  \break
  e'2 e'4 | % 34
  e'4. e'8 fis'8 e'8 | % 35
  d'2. | % 36
  b2. | % 37
  c'2 c'4 | % 38
  c'4. c'8 d'8 c'8 | % 39
  b2. |
  R2. | % 41
  \break
  g2. ~ | % 42
  g4 fis4 g4 | % 43
  b2. | % 44
  e4 fis4 g4 | % 45
  a2. ~ | % 46
  a4 g4 a4 | % 47
  b2. | % 48
  a2. | % 49
  \break
  d'2. |
  des'2 e'4 | % 51
  d'2. ~ | % 52
  d'2. | % 53
  b2. | % 54
  bes2 c'4 | % 55
  b2. ~ | % 56
  b2. | % 57
  \break
  g2. | % 58
  fis2 a4 | % 59
  g2 b4 |
  d'2 c'4 | % 61
  b2. | % 62
  a4 fis4 a4 | % 63
  g2. ~ | % 64
  g2. \bar "|."
}

\include "../muziko.ly"
