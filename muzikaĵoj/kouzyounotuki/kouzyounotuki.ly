\include "../ĉiea.ly"

\header {
  titolo-xx     = "荒城の月"
  titolo-he     = "ירח מעל לטירה החרבה"
  titolo-eo     = "La luno sur la ruina kastelo"
  komponisto-xx = "瀧 廉太郎"
  komponisto-he = "רנטרו טקי"
  komponisto-eo = "Rentarō Taki"
  ikono         = "月"
}

\include "../titolo.ly"

melodio = {
  \key e \phrygianminor
  \time 4/4
  { b b e' fis' } |
  { g' fis' e'2 } |
  { c'4 c' b a } |
  { b2. r4 } |
  \break
  { b b e' fis' } |
  { g' fis' e'2 } |
  { c'4 a b4. b8 } |
  { e2. r4 } |
  \break
  { g4. g8 fis4 e } |
  { c' c' b2 } |
  { a4 b c'4. c'8 } |
  { b2. r4 } |
  \break
  { b b e' fis' } |
  { g' fis' e'2 } |
  { c'4 a b4. b8 } |
  { e2. r4 } |
  \bar "|."
}

\include "../muziko.ly"
