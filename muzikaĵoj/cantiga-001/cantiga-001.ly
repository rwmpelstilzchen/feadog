\include "../ĉiea.ly"

\header {
  titolo-xx     = "Des oge mais quér’ éu trobar"
  titolo-he     = "קנטיגה 1"
  titolo-eo     = "Kantigo 1"
  komponisto-xx = ""
  komponisto-he = "עממי / אלפונזו העשירי"
  komponisto-eo = "popola / Alfonso la 10-a"
  ikono         = "♍"
}

\include "../titolo.ly"

melodio = {
  \key e \minor
  \time 4/4
  b8 b4 b8 b4. a8 |
  b4. c'8 b4. a8 |
  g4 g8 a8 b8 c'8 d'8 c'8 |
  b8 c'8 b8 a8 b4. b8 |
  \break
  d'4 e'4 e'8 d'4 b8 |
  b4 c'16 b16 a8 b4. a8 |
  g4 g4 a8 b8 g8 fis8 |
  e4. d8 e4. e8 |
  \break
  g8 fis8 e8 d8 g4 g8 a8 |
  b8 c'8 b8 a8 b4. b8 |
  d'4 e'4 e'8 d'4 b8 |
  b4 c'16 b16 a8 b4. b16 a16 |
  \break
  g4 g4 a8 b8 g8 fis8 |
  e4. d8 e4. a8 |
  a4. fis8 g16 fis16 g8 a8 fis8|
  fis4. b8 |
  \break
  b4 b8 a8 g4 a8 b8 |
  g8 fis8 e8 fis8 d4. fis8 |
  a4. g16 fis16 e8 fis8 g8 fis8 |
  fis8 e8 e8 d8 e2 |
  \bar "|."
}

\include "../muziko.ly"
