\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "לגדול איתך ביחד: ליל מנוחה"
  titolo-eo     = "Grandiĝi kun vi: Trankvilan nokton"
  komponisto-xx = ""
  komponisto-he = "יעל תלם"
  komponisto-eo = "Jael Telem"
  ikono         = "👀"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key d \major
  \partial 4 d8 e |
  fis4. a8 a fis |
  b4 a b8 a |
  fis4 d d8 fis |
  fis4 e2 |
  \break
  \time 4/4
  a16 a a a fis8 a fis2 |
  a8 fis r4 d8 d d d |
  e2. d8 e |
  \break
  \time 3/4
  fis4. a8 a fis |
  b4 a b8 a |
  fis4. d8 d fis |
  fis4 e fis8 e |
  \break
  d4. d8 d e |
  fis4. b8 a b |
  fis4 d d8 e8 |
  d2. |
  \bar "|."
}

\include "../muziko.ly"
