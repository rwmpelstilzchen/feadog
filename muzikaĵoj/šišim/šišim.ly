\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "בת שישים"
  titolo-eo     = "Sesdek jara"
  komponisto-xx = ""
  komponisto-he = "קובי אושרת"
  komponisto-eo = "Kobi Oŝrat"
  ikono         = "⛰"
}

\include "../titolo.ly"

melodio = {
  \key b \minor
  \time 4/4
  \tempo 4 = 120
  r a8 a a a a a |
  a4 d'2 cis'8 d' |
  e'4 d' cis' b8 g ~ |
  g1 |
  r4 g8 g g g g g |
  g4 e'2 d'8 cis' |
  b4 a8 a ~ a2 |
  r1 |
  r4 fis8 fis fis fis fis fis |
  fis4 d'2 cis'8 d' |
  e'4 dis' e' fis'8 e' ~ |
  e'2. b8 cis' |
  e'4 d' cis' b |
  d' b fis b |
  gis2 ais |
  b d'4 cis'8 d' ~ |
  d'4 b d' cis'8 d' ~ |
  d'4 g d' d' |
  cis' fis b cis'8 d' ~ |
  d'2. cis'8 d' |
  e'4 d' e' d'8 fis' ~ |
  fis'4 d' a fis' |
  e'2 cis' |
  fis'1 |
  \break
  r4 b d' e' |
  fis' b2 d'8 e' |
  fis'4 b2 e'8 fis' |
  g'4 fis' e' fis'8 g' ~ |
  g'2. e'8 fis' |
  g'4 fis' g' fis'8 a' ~ |
  a'4 g' fis' e'8 g' ~ |
  g'4 fis' fis' e'8 fis' ~ |
  fis'4 b d' e' |
  fis' b2 d'8 e' |
  fis'4 b2 e'8 fis' |
  g'4 fis' a' g'8 g' ~ |
  g'2. e'8 fis' |
  a'4 g' fis' e'8 g' ~ |
  g'4 fis' d' b8 fis' ~ |
  fis'4 e' d' cis'8 d' ~ |
  d'2. e'8 fis' |
  a'4 g' e' cis'8 g' ~ |
  g'4 fis' d' b |
  gis b ais cis' |
  b1 \bar "|."
 
}

\include "../muziko.ly"