\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "את עושה אותי אמא"
  titolo-eo     = "Vi faras min patrino"
  komponisto-xx = ""
  komponisto-he = "יהודית רביץ"
  komponisto-eo = "Judit Ravic"
  ikono         = "🤰"
}

\include "../titolo.ly"

\language "italiano"
melodio =  {
  \tempo 4=100
  \key re \major
  \time 6/8
  \partial 16*2 fad16 sol |
  la8. si16 la8 fad4 mi8 |
  fad4 fad8 fad8 sol8 fad8 |
  fad8 re4. re8 fad8 |
  mi2 ~ mi8 fad16 sol |
  \break
  la8. si16 la8 fad4 re16 mi16 |
  fad4 fad8 fad8 sol8 fad8 |
  fad8 re4. ~ re8 fad8 |
  mi2 fad8 fad \bar "||" |
  \break

  sol8 fad8 sol mi4 fad8 |
  sol8 fad sol mi4. ~
  mi2 fad8 fad
  sol8 fad sol4 mi16 fad16 sol8 fad sol8 mi4 fad8 sol fad8 sol mi2 \bar "||" |
  \break
  
  r2 r8 fad8 |
  la8. si16 la8 fad8 fad8 mi8 |
  fad4 fad8 fad8 sol8 fad8 |
  fad8 re2 fad8 |
  mi2 ~ mi8 fad8 |
  \break
  la8 si la8 fad4 re16 mi16 |
  sol8 fad8 ~ fad8 fad8 sol8 fad8 ~ |
  fad8 re4. ~ re8 fad8 |
  mi2 ~ mi8 fad16 fad \bar "||" |
  \break

  sol8 fad8 sol mi4 fad16 fad |
  sol8 fad sol mi4. ~
  mi2 ~ mi16 fad16 fad fad16
  sol8 fad sol8 mi4 fad8 sol8 fad sol8 mi4 fad8 sol fad8 sol la4. \bar "||" |
  \break

  \key fad \minor
  r2 la8 la |
  la8 sold la fad4
  la16 la la8 sold la si fad4 |
  r2 la8 la |
  la8 sold la fad4
  la16 la la8 sold la fad4 sold8 |
  la sold la si8 ~ si8 fad8 ~ |
  fad2. |
  \break

  la8 la re' dod' la la |
  si2. |
  la8 la re' dod' la4 ~ |
  la8 la8 re' dod' la la |
  si2. \bar "|." |
}
\language "nederlands"

\include "../muziko.ly"
