\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "עשר אצבעות"
  titolo-eo     = "Dec fingroj"
  komponisto-xx = ""
  komponisto-he = "דוד זהבי"
  komponisto-eo = "David Zehavi"
  ikono         = "👐"
}

\include "../titolo.ly"

melodio = {
  \key g \major
  \time 4/4
  g4 g g a |
  b4 b b2 |
  c'4 b a g |
  a2 d |
  \break
  a4 a a b |
  c'4 c' c'2 |
  b4 g a fis |
  g2 g |
  \break
  d'4 d' d' b |
  g4 a b2 |
  a4 a d' a |
  d'4 d' d'2 |
  \break
  g4 d g d |
  g4 a b2 |
  a4 a a d |
  g2 g |
  \bar "|."
}

\include "../muziko.ly"
