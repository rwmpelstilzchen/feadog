\include "../ĉiea.ly"

\header {
  titolo-xx     = "Melodies of Life"
  titolo-he     = "נעימוֹת החיים"
  titolo-eo     = "La melodioj de la vivo"
  komponisto-xx = "植松伸夫"
  komponisto-he = "נובואו אואמצו"
  komponisto-eo = "Nobuo Uemacu"
  ikono         = "🎶"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 4/4
  fis' e' d'4. b16 cis' |
  d'8 cis' b a2 e'16 fis' |
  g'8 fis' d' fis' e'4 fis'8 d' |
  bes4 g bes8 a g fis |
  fis d' ~ d'2. ~ |
  d' r8 a |
  \break
  fis'4 fis'8 e' d'4 d'8 cis' |
  b cis'8. d'8 e'16 a4. a8 |
  b cis' d' b b16 a8 d'8. fis'8 |
  g' fis' d' fis' e'4. fis'8 |
  a'4. b'8 a'16 g'8 g'8. fis'8 |
  fis' e'8. e'8 fis'16 g'4. fis'16 e' |
  d'4. a8 b16 d' ~ d'4. |
  e'4 fis' g'8 e'4. ~ |
  e'2 ~ e'4. a8 |
  \break
  fis'4 fis'8 e' d'4 d'8 cis' |
  b cis' d' e' a4. a8 |
  b cis' d' b b16 a8 d'8. fis'8 |
  g' fis'8. d'8 fis'16 e'4. fis'8 |
  a' a' a'8. b'16 a' g'8. g'8 fis' |
  fis' e'8. e'8 fis'16 g'4. fis'16 e' |
  d'4. a8 b16 d' ~ d'4 e'8 |
  fis'16 e'8 d'16 ~ d'2. |
  \break
  r2 g'8 fis' e' d' |
  cis'16 d' e'8 ~ e'2. |
  r4. fis'8 a' g' fis' e' |
  dis' e'16 fis' ~ fis'2. |
  g'4 fis' e'2 |
  r8 fis' g' fis'16 e'8. d'8 cis' d' |
  fis'2. a'8 g' |
  e'2 ~ e'4. a8 |
  \break
  d'4 e'8 fis' cis'2 |
  cis'8 d' cis' a b2 |
  g'8 fis' fis' e' e' d'4 b8 |
  cis' d'16 e' ~ e'2. |
  d'4 e'8 fis' a'4 ~ \tuplet 3/2 { a'8 fis' cis' } |
  b1 |
  g'8 fis' fis' e' e'2 |
  a'8 g'16 g'8. fis'16 fis'8. e'8 ~ \tuplet 3/2 { e'8 fis' g' } |
  a'1 |
  \break
  r4 r8 fis' g' fis' ~ \tuplet 3/2 { fis'8 cis' e' } |
  e'8 d' ~ d'2. |
  e'2 e'8 d'16 cis'8. b8 |
  a4. fis8 a fis e' d' ~ |
  d'4 cis' b a |
  fis'4. a'8 d' fis' e' e'16 d' |
  cis'4. cis'16 d' e'8 d' cis' b |
  a2 r |
  R1 |
  \break
  r16 fis g a b8 a b d' d'4 |
  g8 d' cis'4. b8 a16 g fis8 |
  e d a4. r8 r4 |
  r2 r4 r8 a |
  fis'4 fis'8 e' d'4 d'8 cis' |
  b cis' d' e' a2 |
  b8 cis' d' b b16 a8 d'8. fis'8 |
  g' fis'8. d'8 fis'16 e'4. fis'8 |
  a'4. b'8 a'8. g' fis'8 |
  fis' e'8. e'8 fis'16 g'4. fis'16 e' |
  d'4. a8 d'4 d'16 e' fis'8 |
  e' d'2. r16 r |
  \break
  r2 g'8 fis' e' d' |
  cis'16 d' e'8 ~ e'2. |
  r4. fis'8 a' g' fis' e' |
  dis' e'16 fis' ~ fis'2. |
  g'4 fis' e'2 |
  r8 fis' g' fis'16 e'8. d'8 cis' d' |
  fis'2. a'8 g' |
  e'2 ~ e'4. a8 |
  \break
  d'4 e'8 fis' cis'2 |
  cis'8 d' cis' a b2 |
  g'8 fis' fis' e' e' d'4 b8 |
  cis' d'16 e' ~ e'2. |
  d'4 e'8 fis' a'4 fis'8 cis' |
  b1 |
  g'8 fis' fis' e' e'2 |
  a'8 g'16 g'8. fis'16 fis'8. e'8 ~ \tuplet 3/2 { e'8 fis' g' } |
  a'1 |
  \break
  
  
   r4 r8
    fis'8 g'8  fis'8 ~  \times 2/3  { fis'8  cis'8 e'8  } | 
    e'8  d'8 ~  d'2  a'8   e'8  | 
    cis'8  cis'8 ~ cis'8 d'8  cis'8  a8  b4 ~ | 
    b8  b8 g'8 fis'8  fis'8  e'8 e'8 d'8 ~  | 
    d'8  b8  cis'8  d'16 e'16 ~  e'2 ~ | 
    e'4 d'4 e'8  fis'8  a'4 ~ | 
    \times 2/3  { a'8  fis'8 cis'8  } b2. ~ | 
    b4 g'8  fis'8  fis'8  e'8  e'4 ~ | 
    e'8  g'8  a'8  g'16 g'8. fis'16 fis'8. e'8 ~  |
    \times 2/3  { e'8  fis'8 g'8  } a'2. ~ | 
    a'4 r4 r8 fis'8   g'8 fis'8 ~  | 
    \times 2/3  { fis'8  cis'8 e'8  } e'4 d'8 ^\fermata  a8  d'8 e'8  | 
    fis'8  a'8  e'2 e'8  d'16 cis'16 ~  | 
    cis'8  b8  a4. fis8  a8 fis8  | 
    e'8  d'8 ~  d'4 cis'4 b4 | 
    a4 b2 g'4 ^\markup{ \italic {rit.} } | 
    fis'4 e'4 d'4 cis'4 | 
    a2 d2 ~ | 
    d1 \bar "|."
    }

xmelodio = {
  r4 r8 fis' g' fis' ~ \tuplet 3/2 { fis'8 cis' e' } |
  e'8 d' ~ d'2 a8 e |
  a e fis cis fis cis g a |
  b g g e < g g g > e fis d |
  fis d a e cis' a a a16 b |
  cis' a b cis' fis8 d fis d a cis |
  a cis b a b d' a b, |
  g b, e fis, e fis, e g,16 a, |
  b, a, b, d g,8 fis, g g, b b, |
  d' d r g16 e fis g a fis g a b cis' |
  d' e' fis' g' a'4. \breathe r8 r4 |
  r4. r4 a8 d' e' |
  fis' a' e'2 e'8 d'16 cis' ~ |
  cis'8 b a4. fis8 a fis |
  e' d' ~ d'4 cis' b |
  a b2 g'4^\markup \line { \italic "rit."} |
  fis' e' d' cis' |
  a2 d ~ |
  d1 \bar "|."
 
}


oldmelodio = \transpose e d \relative e' {
  
    \clef "treble" \key e \major \numericTimeSignature\time 4/4 | 
    gis4 ^\markup{ \bold {Adagio} }  fis4 e4. cis16  dis16  | 
    e8  dis8 cis8  b2 fis'16  gis16  | 
    a8  gis8 e8 gis8  fis4 gis8  e8  | 
    c4 a4 c8  b8 a8 gis8   | 
    gis8  e'8 ~  e2. ~ | 
    e2. ^\markup{ \bold {Adagio} } r8 b8  | 
    gis'4 gis8  fis8  e4 e8  dis8  | 
    cis8  dis8. e8 fis16  b,4. b8  | 
    cis8  dis8 e8 cis8  cis16  b8 e8. gis8  | 
    a8  gis8 e8 gis8  fis4. gis8 | 
    b4. cis8 b16  a8 a8. gis8  | 
    gis8  fis8. fis8 gis16  a4. gis16  fis16   | 
    e4. b8 cis16  e16 ~  e4. | 
    fis4 gis4 a8 fis4. ~ | 
    fis2 ~ fis4. b,8  | 
    gis'4 gis8  fis8  e4 e8  dis8  | 
    cis8  dis8 e8 fis8  b,4. b8  | 
    cis8  dis8 e8 cis8  cis16  b8 e8. gis8  | 
    a8  gis8. e8 gis16  fis4. gis8 | 
    b8  b8  b8.  cis16  b16  a8.  a8  gis8  | 
    gis8  fis8. fis8 gis16  a4. gis16  fis16   | 
    e4. b8 cis16  e16 ~  e4 fis8 | 
    gis16  fis8 e16 ~  e2. | 
    r2 a8  gis8 fis8 e8  | 
    dis16  e16 fis8 ~  fis2.  | 
    r4. gis8 b8  a8 gis8 fis8  | 
    eis8  fis16 gis16 ~  gis2. | 
    a4 gis4 fis2 | 
    r8 gis8  a8  gis16 fis8. e8 dis8 e8   | 
    gis2. b8  a8  | 
    fis2 ~ fis4. b,8   | 
    e4 fis8  gis8  dis2 | 
    dis8  e8 dis8 b8  cis2  | 
    a'8  gis8 gis8 fis8  fis8 e4 cis8 | 
    dis8  e16 fis16 ~  fis2. | 
    e4 fis8  gis8  b4 ~ 
    \times 2/3  {
        b8  gis8 dis8  }
    | 
    cis1 | 
    a'8  gis8 gis8 fis8  fis2  | 
    b8  a16 a8. gis16 gis8. fis8 ~  
    \times 2/3  {
        fis8  gis8 a8  }
    | 
    b1 | 
    r4 r8 gis8  a8  gis8 ~  
    \times 2/3  {
        gis8  dis8 fis8  }
    | 
    fis8  e8 ~  e2.  | 
    fis2 fis8  e16 dis8. cis8  | 
    b4. gis8 b8  gis8 fis'8 e8 ~  | 
    e4 dis4 cis4 b4 | 
    gis'4. b8 e,8  gis8  fis8  fis16 e16   | 
    dis4. dis16  e16  fis8  e8 dis8 cis8  | 
    b2 r2 | 
    R1 | 
    r16 gis16   a16 b16  cis8  b8  cis8  e8  e4  | 
    a,8  e'8  dis4. cis8 b16  a16 gis8   | 
    fis8  e8  b'4.  r8 r4 | 
    r2 r4 r8 b8  | 
    gis'4 gis8  fis8  e4 e8  dis8  | 
    cis8  dis8 e8 fis8  b,2  | 
    cis8  dis8 e8 cis8  cis16  b8 e8. gis8  | 
    a8  gis8. e8 gis16  fis4. gis8 | 
    b4. cis8 b8.  a8. gis8  | 
    gis8  fis8. fis8 gis16  a4. gis16  fis16   | 
    e4. b8 e4 e16  fis16 gis8  | 
    fis8 e2. r16 r16 | 
    r2 a8  gis8 fis8 e8  | 
    dis16  e16 fis8 ~  fis2.  | 
    r4. gis8 b8  a8 gis8 fis8  | 
    eis8  fis16 gis16 ~  gis2. | 
    a4 gis4 fis2  | 
    r8 gis8 a8  gis16 fis8. e8 dis8 e8  | 
    gis2.  b8  a8  | 
    fis2 ~ fis4. b,8   | 
    e4 fis8  gis8  dis2  | 
    dis8  e8 dis8 b8  cis2 | 
    a'8  gis8 gis8 fis8  fis8 e4 cis8 | 
    dis8  e16 fis16 ~  fis2. | 
    e4 fis8  gis8  b4 gis8  dis8   | 
    cis1 | 
    a'8  gis8 gis8 fis8  fis2 | 
    b8  a16 a8. gis16 gis8. fis8 ~  
    \times 2/3  {
        fis8  gis8 a8  }
    | 
    b1  | 
    r4 r8 gis8 a8  gis8 ~  
    \times 2/3  {
        gis8  dis8 fis8  }
    | 
    fis8  e8 ~  e2 b8   fis8  | 
    b8  fis8 gis8 dis8  gis8  dis8 a'8 b8  | 
    cis8  a8 a8 fis8  <a a a>8  fis8 gis8 e8  | 
    gis8  e8 b'8 fis8  dis'8  b8  b8  b16 cis16   | 
    dis16  b16 cis16 dis16  gis,8  e8  gis8  e8 b'8 dis,8  | 
    b'8  dis,8 cis'8 b8  cis8  e8 b8 cis,8  | 
    a'8  cis,8 fis8 gis,8  fis'8  gis,8  fis'8  a,16 b16  | 
    cis16  b16 cis16 e16  a,8  gis8  a'8  a,8 cis'8 cis,8   | 
    e'8  e,8  r8 a16  fis16  gis16  a16 b16 gis16  a16  b16 cis16 dis16  | 
    e16  fis16 gis16 a16  b4. \breathe r8 r4 | 
    r4. r4 b,8   e8 fis8  | 
    gis8  b8  fis2 fis8  e16 dis16 ~   | 
    dis8  cis8  b4. gis8  b8 gis8  | 
    fis'8  e8 ~  e4 dis4 cis4 | 
    b4 cis2 a'4 ^\markup{ \italic {rit.} } | 
    gis4 fis4 e4 dis4 | 
    b2 e,2 ~ | 
    e1 \bar "|."
    }

\include "../muziko.ly"
