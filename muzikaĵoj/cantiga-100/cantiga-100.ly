\include "../ĉiea.ly"

\header {
  titolo-xx     = "Santa Maria Stela do dia"
  titolo-he     = "קנטיגה 100"
  titolo-eo     = "Kantigo 100"
  komponisto-xx = ""
  komponisto-he = "עממי / אלפונזו העשירי"
  komponisto-eo = "popola / Alfonso la 10-a"
  ikono         = "♍"
}

\include "../titolo.ly"

melodio = {
  \key e \dorian
  \time 4/4
  b4 a8 g  fis4 fis8 g |
  a4 b  e e |
  b4 a8 g  fis4 e8 fis |
  g8 fis e d  e4 e |
  \break
  a4 b  cis'4. b8 |
  d'8 cis' b cis'  a4 a |
  a4 b  cis'4. b8 |
  d'8 cis' b a  b4 b |
  \break
  cis'4 d'  e'4. cis'8 |
  d'8 e' d'16 cis' b8  cis'4 cis' |
  cis'4 cis'  cis'4. b8 |
  d'8 cis' b cis' a4 a |
  \break
  b4 a8 g  fis4 fis8 g |
  a4 b  e e |
  b4 a8 g  fis4 e8 fis |
  g8 fis e d  e4 e |
  \bar "|."
}

\include "../muziko.ly"
