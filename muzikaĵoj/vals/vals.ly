\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ולס להגנת הצומח"
  titolo-eo     = "Vals por la konservado de la plantaro"
  komponisto-xx = ""
  komponisto-he = "נעמי שמר"
  komponisto-eo = "Naomi Ŝemer"
  ikono         = "¾"
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 240
  \time 3/4
  \key g \major
  b r d |
  b2. ~ |
  b4 a g |
  b r r |
  r e e |
  b2 c'4 |
  b2. |
  a |
  \break a4 r d |
  a2. ~ |
  a4 g fis |
  a r r |
  r d d |
  a2 b4 |
  g r r |
  R2. |
  \break b2 d4 |
  b2. ~ |
  b4 a g |
  b2. ~ |
  b4 b b |
  e'2 b4 |
  b2. |
  a |
  \break a2 d4 |
  a2. ~ |
  a4 g fis |
  a r r |
  r d d |
  a2 b4 |
  g r r |
  R2. |
  \break
  g'2. |
  fis' |
  e' ~ |
  e'4 fis' g' |
  fis'2. ~ |
  fis'2 cis'4 |
  e'2. |
  d' |
  \break
  f' |
  e' |
  d' ~ |
  d'4 e' f' |
  e'2. ~ |
  e'2 d'4 |
  c' r r |
  R2. |
  \break
  c' |
  b |
  a ~ |
  a4 b c' |
  d'2. ~ |
  d'2 d'4 |
  g'2. |
  g |
  \break
  a4 r r |
  r r c' |
  b r r |
  r d b |
  a r r |
  r r d' |
  g r r |
  R2._\markup{\italic{"D.C. × 2"}} |
  \bar "||"
  \pageBreak
  g' |
  fis' |
  e' |
  e'4 d' e' |
  cis'2. |
  fis'2 cis'4 |
  e'2. |
  d' |
  \break
  f'2 f'4 |
  e'2 e'4 |
  d'2 d'4 |
  d'2 f'4 |
  e'2 f'4 |
  e' b d' |
  c' r r |
  R2. |
  \break
  c' |
  b |
  a ~ |
  a4 b c' |
  d'2. ~ |
  d'2 d'4 |
  g'2. |
  g |
  \break
  a4 r r |
  r r c' |
  b r r |
  r d b |
  a r r |
  r r d' |
  g r r |
  R2. |
  \bar "|."
}

\include "../muziko.ly"