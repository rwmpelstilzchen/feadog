\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "השקדיה פורחת"
  titolo-eo     = "La migdalarbo floras"
  komponisto-xx = ""
  komponisto-he = "מנשה רבינא"
  komponisto-eo = "Menaŝe Ravina"
  ikono         = "🌸"
}

\include "../titolo.ly"

melodio = {
   \key d \major
   \time 4/4
   \partial 8 {a8} |
   d8 d8 d8 fis8 a4 fis8 fis8 |
   a8 a8 b8 b8 a4 fis4 |
   \break
   d8 e8 fis8 d8 g8 b8 a4 |
   a8 a8 b8 a8 fis8 a8 d4 |
   \break
   d'8 d'8 b8 b8 a4 fis4 |
   g8 g8 fis8 e8 a2 |
   \break
   d'8 d'8 b8 b8 a4 fis4 |
   g8 g8 fis8 e8 d2 |
   \bar "|."
}

\include "../muziko.ly"