\include "../ĉiea.ly"

\header {
  titolo-xx     = "魔女の宅急便：海の見える町"
  titolo-he     = "קיקי: עיר ליד הים"
  titolo-eo     = "Urbo apud la maro"
  komponisto-xx = ""
  komponisto-he = "ג׳ו היסאישי"
  komponisto-eo = "Ĝo Hisaiŝi"
  ikono         = "🐈"
}

\include "../titolo.ly"

melodio =  {
  \key b \minor
  \time 4/4
  r8 fis'8 ( e'8 d'8 ) d'8 ( cis'8 b8 cis'8 ) | % 2
  e'8 ( d'8 cis'8 d'8 ) a'4. ( g'8 ) | % 3
  fis'4. ( e'8 ) e'4. ( d'8 ) | % 4
  e'1 | % 5
  \break
  fis'2. r8 fis8 | % 6
  d'4 -. r8 fis8 cis'4 -. r8 fis8 | % 7
  b4 -- a8 -. g8 -. a4. -- d8 -. | % 8
  e8 ( g8 ) b8 -. d'8 -. cis'8 ( a8 ) fis8 -. e8 -. | % 9
  \break
  fis2. r8 fis8 | \barNumberCheck #10
  d'4 -. r8 fis8 cis'4 -. r8 fis8 | % 11
  b4 -- a8 -. g8 -. a4. -- fis8 -. | % 12
  e8 ( g8 ) b8 -. d'8 -. e'8 ( fis'8 ) cis'8 -. a8 -. | % 13
  \break
  b2. r8 fis8 | % 14
  d'4 -. r8 fis8 cis'4 -. r8 fis8 | % 15
  b4 -- a8 -. g8 -. a4. -- d8 -. | % 16
  e8 ( g8 ) b8 -. d'8 -. cis'8 ( a8 ) fis8 -. e8 -. | % 17
  \break
  fis2. r8 fis8 | % 18
  d'4 -. r8 fis8 cis'4 -. r8 fis8 | % 19
  b4 -- a8 -. g8 -. a4. -- fis8 -. | \barNumberCheck #20
  e8 ( g8 ) b8 -. d'8 -. e'8 ( fis'8 ) cis'8 -. a8 -. | % 21
  \break
  b2. b8 ( cis'8 ) | % 22
  d'8 d'8 d'8 d'8 d'8 cis'8 b8 cis'8 | % 23
  a2. r8 g16 ( a16 ) | % 24
  b8 b8 b8 b8 b8 a8 g8 a8 | % 25
  \break
  fis2. r8 fis8 -- | % 26
  dis8 ( e8 g8 b8 ) a4. g8 | % 27
  eis8 ( fis8 a8 cis'8 ) e'8 ( d'8 cis'8 d'8 ) | % 28
  fis'4. e'16 ( fis'16 ) b4. a16 ( b16 ) | % 29
  fis4. e16 ( fis16 ) b2 | \barNumberCheck #30
  \time 2/4  cis'4. -- fis8 | % 31
  \break
  \time 4/4  d'4 -. r8 fis8 cis'4 -. r8 fis8 | % 32
  b4 -- a8 -. g8 -. a4. d8 | % 33
  e8 ( g8 ) b8 -. d'8 -. cis'8 ( a8 ) fis8 -. e8 -. | % 34
  fis2. r8 fis8 | % 35
  \break
  d'4 -. r8 fis8 cis'4 -. r8 fis8 | % 36
  b4 -- a8 -. g8 -. a4. -- fis8 -. | % 37
  e8 ( g8 ) b8 -. d'8 -. e'8 ( fis'8 ) cis'8 -. a8 -. | % 38
  b2. r4 | % 39
  \break
  r8 b'16 ( ais'16 b'8 ) -. g'16 ( fis'16 g'8 ) -. e'16 ( d'16 e'8 ) -. r8 | \barNumberCheck #40
  r8 a'16 ( g'16 a'8 ) -. fis'16 ( e'16 fis'8 ) -. d'16 ( cis'16 d'8 ) -. r8 | % 41
  r8 g'16 ( fis'16 g'8 ) -. e'16 ( d'16 e'8 ) -. cis'16 ( bis16 cis'8 ) -. r8 | % 42
  r8 fis'16 ( g'16 fis'16 e'16 d'16 cis'16 d'8 ) r8 r4 | % 43
  \break
  r8 b'16 ( ais'16 b'8 ) -. g'16 ( fis'16 g'8 ) -. e'16 ( d'16 e'8 ) -. r8 | % 44
  r8 a'16 ( g'16 a'8 ) -. fis'16 ( e'16 fis'8 ) -. d'16 ( cis'16 d'8 ) -. r8 | % 45
  r8 g'16 ( fis'16 g'8 ) -. e'16 ( d'16 e'8 ) -. cis'16 ( bis16 cis'8 ) -. r8 | % 46
  r8 fis'16 ( g'16 fis'16 e'16 d'16 cis'16 d'8 ) r8 r4 | % 47
  \break
  r8 d'16 ( cis'16 d'8 ) -. fis8 -. g16 ( a16 g16 fis16 ) g8 r8 | % 48
  r8 cis'16 ( b16 cis'8 ) -. e8 -. fis16 ( g16 fis16 e16 ) fis8 r8 | % 49
  r8 b'16 ( ais'16 b'8 ) -. d'8 -. e'16 ( fis'16 e'16 dis'16 ) e'8 r8 | \barNumberCheck #50
  r8 ais'16 ( gis'16 ais'8 ) -. cis'8 -. d'16 ( e'16 d'16 cis'16) b16 ( cis'16 d'16 fis'16 ) | % 51
  a'16 ( gis'16 fis'16 e'16 ) dis'16 ( cis'16 b16 a16 ) g8 ( fis8 ) -. e8 -. d8 -. | % 52
  e4 fis'4 b2 ~ | % 53
  b1 \bar "|."
}

\include "../muziko.ly"
