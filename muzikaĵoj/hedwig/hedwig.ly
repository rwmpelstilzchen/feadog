\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הארי פוטר: הנושא של הדוויג"
  titolo-eo     = "Harry Potter: la temo de Hedwig"
  komponisto-xx = ""
  komponisto-he = "ג׳ון וויליאמס"
  komponisto-eo = "John Williams"
  ikono         = "⚡"
}

\include "../titolo.ly"

melodio =  {
  \key d \major
  \time 3/4 |
  \partial 4 fis4 |
  b4. d'8 cis'4 |
  b2 fis'4  |
  e'2. |
  cis'2.  |
  b4.  d'8 cis'4 |
  a2 c'4  |
  fis2. ~ |
  fis2  fis4  | 
  b4. d'8 cis'4 |
  b2 fis'4   |
  a'2 gis'4 |
  g' ?2  dis'4  |
  g'4. fis'8 eis'4 |
  eis2 d'4  |
  b2. ~ |
  b2 d'4 \bar "||"
  \break

  fis'2 d'4 |
  fis'2 d'4 | 
  g'2 fis'4 |
  eis'2  cis'4 |
  d'4. fis'8 eis'4 |
  eis2 fis4   |
  fis'2. ~ |
  fis'2 d'4  |
  fis'2 d'4 |
  fis'2 d'4 |
  a'2 gis'4 |
  g' ?2  dis'4  | 
  g'4. fis'8 eis'4 |
  eis2  d'4  |
  b2. ~ |
  b2. ~ |
  b2. ~ |
  b2. \bar "||"
  \break

  %\key ? \minor
  \time 2/2  |
  e'8 \!   e'8 -. e'8 -. e'8 -.  e'8 -.  e'8 -. dis'8 -. e'8  -.  |
  f'4  e'8 -.  dis'8 -.  e'4 -. c'4 -.  |
  d'4 -- d'8 -.  cis'8 -.  d'4 f4 |
  e8 -. r8 a8 -.   c'8 -.  dis'2  |
  e'8 -.   e'8 -. e'8 -. e'8 -.  e'8 -.  e'8 -. dis'8 -. e'8  -.  |
  g'4 fis'8 -.  e'8 -.  fis'4 -. bes4 -. | 
  fis'4  e'8 -.  dis'8 -.  e'4 -. a4 -. |
  c'2 e'2 |
  gis'2 e'2 |
  r4 e'4 -. a'4 -.  r4 \bar "|."
}

\include "../muziko.ly"
