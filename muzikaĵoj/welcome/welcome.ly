\include "../ĉiea.ly"

\header {
  titolo-xx     = "You’re Welcome"
  titolo-he     = "מואנה: בכיף שלך"
  titolo-eo     = "Moana: Ne dankinde"
  komponisto-xx = ""
  komponisto-he = "לין־מנואל מירנדה"
  komponisto-eo = "Lin-Manuel Miranda"
  ikono         = "🦈"
}

\include "../titolo.ly"

#(set-global-staff-size 27)

melodio =  {
    \key d \major
	\time 4/4 |
    e'4 a8  a8  a8  a8 a8 a8  |
    g4 r4 r4 r8 g8  |
    g'8  g'8 g'8 g'8  g'8  fis'8  \times 2/3 { e'8 d'4 }  |
    cis'4 r8 fis8 a8  d'8 a'8 fis'8 ~  |
    fis'8  e'8 d'8 e'8 ~  e'4 r4  |
    r2 r4 \times 2/3 { r8 g8  g8 } |
    g'8  g'8 g'8 fis'8  g'8  fis'8 
    \times 2/3  { e'8  d'8 e'8 ~ } |
    e'4 r8 r8 a8  cis'8 a'8 fis'8 ~  |
    fis'4. e'8 ~ e'4 cis'8  e'8 ~   |
    e'4. r8 g'8  g'8 g'8 g'8  |
    g'4 e'8  d'8 
    \times 2/3  {
        cis'8  d'8 e'8 ~  }
    e'4 |
    r4 r8 g8 a8  cis'8 a'8 fis'8 ~  |
    fis'4 e'8  e'8 ~  e'4 cis'8  e'8 ~   |
    e'2 r4 a8  a8  |
    g'8  g'8 g'8 g'8  
    \times 2/3  {
        g'8  fis'8 e'8 ~  }
    e'4 \bar "||" |
	\break

    <fis>4  -. r4 fis8  a8 fis8 cis'8 ~  |
    cis'4 b8 a4. e8  cis'8 ~  |
    cis'8 a2 r8 
    \times 2/3  {
        a8  a8 gis8 ~  }
     |
    gis4 gis8 a4. gis8  fis8 ~  |
    fis4 <fis>4 -. fis8  a8 fis8 cis'8 ~  |
    cis'8  b8  b8 a4. e8  cis'8 ~  |
    cis'8 a2 r8 r8 b8 |
    b8  b8 b8 b8  b8  b8  
    \times 2/3  {
        a8  b8 cis'8  }
    \bar "||" |
	\break

    cis'4 cis'4 \times 2/3 {
        a4 cis'8 }
    \times 2/3  {
        a4 cis'8 ~ }
     |
    cis'4  \times 2/3 {
        a4 cis'8 ~ }
    \times 2/3  {
        cis'4 cis'8 }
    \times 2/3  {
        a4 d'8 ~ }
    |
    d'4 \times 2/3 {
        r8 r8 a8 }
    \times 2/3  {
        d'4 d'8 }
    \times 2/3  {
        d'4 d'8 }
    |
    e'4 d'4 \times 2/3 {
        e'4 d'8 ~ }
    \times 2/3  {
        d'4. }
    |
    r4 \times 2/3 {
        cis'4 cis'8 }
    \times 2/3  {
        cis'4 cis'8 ~ }
    \times 2/3  {
        cis'4 cis'8 ~ }
    |
    cis'4 \times 2/3 {
        cis'4 cis'8 ~ }
    \times 2/3  {
        cis'4 e'8 ~ }
    \times 2/3  {
        e'4 e'8 ~ }
    |
    \times 2/3  {
        e'4 e'8 }
    \times 2/3  {
        d'4 cis'8 }
    \times 2/3  {
        b4 r8 }
    \times 2/3  {
        r8 r8 b8 }
     |
    \times 2/3  {
        e'4 d'8 }
    \times 2/3  {
        cis'4 b8 }
    \times 2/3  {
        cis'4. }
    r4 |
     |
    a'2 \times 2/3 {
        a4 cis'8 }
    \times 2/3  {
        a'4 fis'8 ~ }
    |
    \times 2/3  {
        fis'4. ~ }
    \times 2/3  {
        fis'4 e'8 ~ }
    e'4 \times 2/3 {
        d'4 e'8 ~ }
    |
    e'4 \times 2/3 {
        r8 r8 e8 }
    \times 2/3  {
        e4 e8 }
    \times 2/3  {
        r8 r8 a8 }
    |
    \times 2/3  {
        g'4 g'8 }
    \times 2/3  {
        g'4 g'8 }
    \times 2/3  {
        g'4 fis'8 ~ }
    \times 2/3  {
        fis'4 e'8 ~ }
     |
    e'2 \times 2/3 {
        a4 a8 }
    \times 2/3  {
        a'4 fis'8 ~ }
    |
    \times 2/3  {
        fis'4. ~ }
    \times 2/3  {
        fis'4 e'8 ~ }
    e'4 \times 2/3 {
        d'4 e'8 ~ }
    |
    e'4 \times 2/3 {
        r8 r8 e'8 }
    \times 2/3  {
        e'4 e'8 }
    \times 2/3  {
        r8 r8 e'8 }
    |
    \times 2/3  {
        g'4 g'8 }
    \times 2/3  {
        g'4 g'8 }
    \times 2/3  {
        g'4 fis'8 ~ }
    \times 2/3  {
        fis'4 e'8 }
    \bar "||" |
	\break

    r2  \times 2/3 {
        fis4
    a8 }
\times 2/3  {
    fis4 cis'8 ~ }
 |
cis'4 b8.  a16 ~  a4 e8.  cis'16 ~  |
cis'8.  a16 ~  a8 ~ a4 e8  fis8 gis8 ~  |
gis4 \times 2/3 {
    gis4 a8 ~ }
\times 2/3  {
    a4. }
\times 2/3  {
    gis4 fis8 ~ }
|
fis2 fis8.  a16  fis8.  cis'16 ~  |
cis'4 b8.  a16 ~  a4 e8.  cis'16 ~  |
cis'8.  a16 ~  a8 ~ a4 r4 e8  |
\times 2/3  {
    gis4 gis8 }
\times 2/3  {
    gis4 gis8 }
\times 2/3  {
    gis4 gis8 }
\times 2/3  {
    fis8  gis8 cis'8  }
|
r4 r4 r4 \times 2/3 {
    fis'4 e'8 ~ }
|
\times 2/3  {
    e'4 cis'8 ~ }
cis'4. r8 \times 2/3 {
    e4 cis'8 ~ }
|
\times 2/3  {
    cis'4 a8 ~ }
a4. r8 r4 |
a4 \times 2/3 {
    cis'4 b8 }
\times 2/3  {
    a8  fis8 e8  }
r4 \bar "||" |
\break

\interim
a4 
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {a8  g8 a8  } |
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  b8 cis'8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  g8 a8  }
|
\times 2/3  {
    r4 g8 }
\times 2/3  {
    a4 g8 }
\times 2/3  {
    a4 g8 }
a4 |
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 g8  }
a4 |
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a4 a8 }
\times 2/3  {
    a8  b8 cis'8  }
a4  |
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 g8  }
a4 |
\times 2/3  {
    a4 a8 }
\times 2/3  {
    a4 a8 }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
|
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
|
\times 2/3  {
    r8 a8  a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
a4  |
\times 2/3  {
    r8 a8  a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
a4 |
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8 a4 }
|
\times 2/3  {
    a8 a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
|
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
\times 2/3  {
    a8  a8 a8  }
|
R1*2 \bar "||" |
\deinterim
\break

<fis>4 -. r4 fis8  a8 fis8 cis'8 ~  |
cis'4 b8 a4. e8  cis'8 ~  |
cis'8 a2 r8 
\times 2/3  {
    a8  a8 gis8 ~  }
|
gis4 gis8 a4. gis8  fis8 ~  |
fis4 <fis>4 -. fis8  a8 fis8 cis'8 ~  |
cis'8  b8  b8 a4. e8  cis'8 ~   |
cis'8 a2 r8 r8 b8 |
b8  b8 b8 b8  b8  b8  
\times 2/3  {
    a8  b8 cis'8  }
|
<a>4  -. r4 fis8  a8 fis8 cis'8 ~  |
cis'4 b8 a4. e8  cis'8 ~  |
cis'8 a2 r8 
\times 2/3  {
    a8  a8 gis8 ~  }
|
gis4 gis8 a4. gis8  fis8 ~   |
fis4 <fis>4 -. fis8  a8 fis8 cis'8 ~  |
cis'8  b8  b8 a4. e8  cis'8 ~  |
cis'8 a2 r8 r8 b8 |
b8  b8 b8 b8  b8  b8 
\times 2/3  {
    a8  b8 cis'8  }
|
fis'4 fis'4 -> r8 r8 fis'8  e'8 ~  |
e'8 cis'2 r8 e8  cis'8 ~  |
cis'8 a4. r2  \bar "|." |
}

\include "../muziko.ly"
