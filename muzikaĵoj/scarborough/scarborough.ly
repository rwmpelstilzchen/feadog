\include "../ĉiea.ly"

\header {
  titolo-xx     = "Scarborough Fair"
  titolo-he     = "יריד סקרבורו"
  titolo-eo     = "La foiro de Skarboro"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🌿"
}

\include "../titolo.ly"

melodio = {
   \key e \minor
   \time 3/4
   e2 e4 |
   b2 b4 |
   fis4. g8 fis4 |
   e2. |
   b2 d'4 |
   e'2 d'4 |
   b4 cis'4 a4 |
   b2 b4 |
   \break
   e'2 e'4 |
   d'2 b4 |
   b4 a4 g4 |
   fis8 e8 d2 |
   e2 b4 |
   a2 g4 |
   fis4 e4 d4 |
   e2. |
   \bar "|."
}

\include "../muziko.ly"
