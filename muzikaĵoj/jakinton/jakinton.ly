\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "פזמון ליקינתון"
  titolo-eo     = "Kanto al la hiacinto"
  komponisto-xx = ""
  komponisto-he = "רבקה גְּוִילי"
  komponisto-eo = "Rivka Gvili"
  ikono         = "🌛"
}

\include "../titolo.ly"

melodio = {
   \key g \major
   \time 3/4
   g4 b4 d'4 b4 
   a4 g4 e4 g4 
   a4 a8 b8 g2
   \break
   g4 b4 d'4 b4 
   e'4 d'4 a4 b4 
   d'4 d'8 a8 d'2
   \break
   \time 4/4
   a8 b8 c'4 a8 b8 c'4 
   d'4 d'4 b2
   g8 a8 b4 g8 a8 b4 
   b4 a4 e2
   \break
   \time 3/4
   d4 g4 a4 b4 
   a4 g4 e4 g4 
   a4 a8 b8 g2
   \bar "|."
}

\include "../muziko.ly"
