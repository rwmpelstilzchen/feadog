\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מאחורי ההר"
  titolo-eo     = "Malantaŭ la monto"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "💬"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 4/4
  d4 d d d |
  fis4 a fis2 |
  a2 b2 |
  a1 |
  \break
  g4 g g g |
  fis4 fis fis2 |
  e2 a2 |
  d1 |
  \break
  d4 d d fis |
  a4 a a2 |
  b2 b |
  a1 |
  \break
  g4 g g g |
  fis4 fis fis2 |
  e2 a2 |
  d1 |
  \bar "|."
}

\include "../muziko.ly"
