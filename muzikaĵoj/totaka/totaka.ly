\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "השיר של טוטקה"
  titolo-eo     = "La kanto de Totaka"
  komponisto-xx = "戸高一生"
  komponisto-he = "קזומי טוטקה"
  komponisto-eo = "Kazumi Totaka"
  ikono         = "🐣"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 6/8
  d d16 e fis4 e8 |
  d4. a |
  fis d' |
  a r |
  \break
  a4 a16 bes a4 gis8 |
  f4. r |
  e4. a |
  d4. r |
}

\include "../muziko.ly"
