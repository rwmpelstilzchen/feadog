\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מה עם המים?"
  titolo-eo     = "Kio okazis pri la akvo?"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "🚰"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \major
  \partial 8 a8 |
  fis8 a a a g b4 b8 |
  a8 a b cis' d'4 a |
  fis'4 d' b8. b16 e'8 e'|
  d'8 cis' b a fis'4 d'8 d' |
  \break
  g'8 g' e' g' fis'8. d'16 d'8 fis' |
  e'8 e' d' e' fis'4 d'8 fis' |
  e'8 e' e' e' e' b cis' d' |
  e'2 ~ e'8 r8 r4 |
  \break
  d'4 a8 a b4 a |
  d'8 d' a a b4 a |
  d'8 d' cis' d' e'4 b8 b |
  d'8 cis' b cis' d'4 r |
  \break
  d'8 d' d' a b b a a |
  d'8 d' d' a b b a a |
  d'8 d' cis' d' e'4 r8 b |
  d'8 cis' b cis' d'4 r |
  \bar "|."
}

\include "../muziko.ly"
