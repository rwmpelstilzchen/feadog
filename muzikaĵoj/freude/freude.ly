\include "../ĉiea.ly"

\header {
  titolo-xx     = "An die Freude"
  titolo-he     = "אוֹדָה לשמחה"
  titolo-eo     = "Odo al ĝojo"
  komponisto-xx = ""
  komponisto-he = "לודוויג ון בטהובן"
  komponisto-eo = "Ludwig van Beethoven"
  ikono         = "☺"
}

\include "../titolo.ly"

melodio =  {
  \key g \major
  \time 4/4
  b4 b4 c'4 d'4 |
  d'4 c'4 b4 a4 |
  g4 g4 a4 b4 |
  b4. a8 a2 |
  \break
  b4 b4 c'4 d'4 |
  d'4 c'4 b4 a4 |
  g4 g4 a4 b4 |
  a4. g8 g2 |
  \break
  a4 a4 b4 g4 |
  a4 b8 c'8 b4 g4 |
  a4 b8 c'8 b4 a4 |
  g4 a4 d2 |
  \break
  b4 b4 c'4 d'4 |
  d'4 c'4 b4 a4 |
  g4 g4 a4 b4 |
  a4. g8 g2 |
  \bar "|."
}

\include "../muziko.ly"
