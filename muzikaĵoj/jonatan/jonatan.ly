\include "../ĉiea.ly"

\header {
  titolo-xx     = "Hänschen klein"
  titolo-he     = "יונתן הקטן"
  titolo-eo     = "Hanseto la malgranda"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🌳"
}

\include "../titolo.ly"

melodio = {
   \key d \major
   \time 4/4
   a4 fis4 fis2|
   g4 e4 e2|
   d4 e4 fis4 g4|
   a4 a4 a2|
   \break
   a4 fis4 fis2|
   g4 e4 e2|
   d4 fis4 a4 a4|
   d2 r2|
   \break
   e4 e4 e4 e4|
   e4 fis4 g2|
   fis4 fis4 fis4 fis4|
   fis4 g4 a2|
   \break
   a4 fis4 fis2|
   g4 e4 e2|
   d4 fis4 a4 a4|
   d1|
   \bar "|."
}

\include "../muziko.ly"
