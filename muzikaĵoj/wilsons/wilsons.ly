\include "../ĉiea.ly"

\header {
  titolo-xx = ""
  titolo-he = "וילסון׳ז ויילד"
  titolo-eo = "Wilson’s Wilde"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono = "❦"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 3/4
  \partial 4 a4 |
  d'2 a4 |
  fis4 d g |
  fis4. e8 d4 |
  e2 a4 |
  d'2 a4 |
  fis4 d g |
  e4. d8 e4 |
  d2 a4 |
  \break
  d'2 a4 |
  fis4 d g |
  fis4 g8 fis e d |
  e2 a4 |
  d'2 a4 |
  fis4 d8 e fis g |
  e8 d %{cis%}e d e %{cis%}fis |
  < d %{d,%} >2. |
  \bar "||"

  \break
  fis4. g8 a4 |
  a4. b8 a4 |
  b4. cis'8 d'4 |
  cis'2 a4 |
  fis8 e fis g a4 |
  a8 g a b a4 |
  b8 a b cis' d'4 |
  cis'2 a4 |
  \bar "||"
  \break

  fis4 a2 |
  g4 b2 |
  fis4 a2 |
  e4 g2 |
  fis4 a2 |
  g4 b2 |
  fis2 < e >4 |
  d2. |
  \bar "||"
  \break

  fis8 g a2 |
  g8 a b2 |
  fis8 g a2 |
  e8 fis g2 |
  fis8 g a2 |
  g8 a b2 |
  fis4 e8 d %{cis4%}e4 |
  d2.
  \bar "|."
}

\include "../muziko.ly"
